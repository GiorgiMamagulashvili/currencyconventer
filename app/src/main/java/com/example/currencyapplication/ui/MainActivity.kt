package com.example.currencyapplication.ui

import android.graphics.Color
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.currencyapplication.R
import com.example.currencyapplication.databinding.ActivityMainBinding
import com.example.currencyapplication.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        arrayAdapters()

        binding.convertBt.setOnClickListener {
            viewModel.convert(
                binding.amountEditText.text.toString(),
                binding.spinnerTxt2.text.toString(),
                binding.spinnerTxt1.text.toString()
            )
        }

        lifecycleScope.launchWhenCreated {
            viewModel.conversion.collect { event ->
                when (event) {
                    is MainViewModel.CurrentEvent.Success -> {
                        binding.resultText.apply {
                            setTextColor(Color.BLACK)
                            text = event.resultText
                        }

                    }
                    is MainViewModel.CurrentEvent.Failure -> {
                        binding.resultText.apply {
                            setTextColor(Color.BLACK)
                            text = event.errorText
                        }
                    }
                    is MainViewModel.CurrentEvent.Loading -> {

                    }
                    else -> Unit
                }
            }
        }
    }

    private fun arrayAdapters() {
        val currency = resources.getStringArray(R.array.currency_codes)
        val arrayAdapter = ArrayAdapter(this, R.layout.dropdown_item, currency)
        binding.spinnerTxt1.setAdapter(arrayAdapter)
        binding.spinnerTxt2.setAdapter(arrayAdapter)
    }
}