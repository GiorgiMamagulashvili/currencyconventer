package com.example.currencyapplication.ui.splash

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.currencyapplication.ui.MainActivity
import com.example.currencyapplication.R
import com.example.currencyapplication.databinding.ActivitySplashBinding

class SplashActivity : AppCompatActivity() {

    lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setAnimations()
        setBackgroundAnimation()

        binding.fab.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)
            finish()
        }

    }

    private fun setBackgroundAnimation(){
        val animationDrawable:AnimationDrawable = binding.splashActivityLayout.background as AnimationDrawable
        animationDrawable.apply {
            setExitFadeDuration(3000)
            setEnterFadeDuration(1000)
            start()
        }
    }

    private fun setAnimations() {
        val imageSplashAnimation = AnimationUtils.loadAnimation(this, R.anim.anim_splash_image)
        binding.ivWallet.animation = imageSplashAnimation

        imageSplashAnimation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                //
            }

            override fun onAnimationEnd(animation: Animation?) {
                binding.fab.isVisible = true
                binding.txt.apply {
                    startAnimation(
                        AnimationUtils.loadAnimation(
                            applicationContext,
                            R.anim.fade
                        )
                    )
                    isVisible = true
                }
                binding.fab.apply {
                    startAnimation(
                        AnimationUtils.loadAnimation(
                            applicationContext,
                            R.anim.fade
                        )
                    )
                }
            }

            override fun onAnimationRepeat(animation: Animation?) {
                //
            }

        })
    }

}