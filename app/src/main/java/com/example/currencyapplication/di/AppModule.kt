package com.example.currencyapplication.di

import com.example.currencyapplication.main.DefaultMainRepository
import com.example.currencyapplication.main.MainRepository
import com.example.currencyapplication.retrofit.CurrencyAPI
import com.example.currencyapplication.util.Constants.BASE_URL
import com.example.currencyapplication.util.DispatcherProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideCurrencyApi(): CurrencyAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CurrencyAPI::class.java)

    @Provides
    fun provideMainRepository(api: CurrencyAPI): MainRepository = DefaultMainRepository(api)

    @Provides
    fun provideDispatchers():DispatcherProvider = object :DispatcherProvider{
        override val main: CoroutineDispatcher
            get() = Dispatchers.Main
        override val io: CoroutineDispatcher
            get() = Dispatchers.IO
        override val default: CoroutineDispatcher
            get() = Dispatchers.Default
        override val unconfined: CoroutineDispatcher
            get() = Dispatchers.Unconfined

    }
}