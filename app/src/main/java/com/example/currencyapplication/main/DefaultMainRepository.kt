package com.example.currencyapplication.main

import com.example.currencyapplication.retrofit.CurrencyAPI
import com.example.currencyapplication.retrofit.model.CurrencyResponse
import com.example.currencyapplication.util.Resource
import java.lang.Exception
import javax.inject.Inject

class DefaultMainRepository @Inject constructor(
    private val api:CurrencyAPI
) : MainRepository {
    override suspend fun getCurrency(base: String): Resource<CurrencyResponse> {
        return try {
            val response = api.getCurrency(base)
            val result = response.body()
            if (response.isSuccessful && result != null){
                Resource.Success(result)
            }else{
                Resource.Error(response.message())
            }
        }catch (e:Exception){
            Resource.Error(e.message ?: "An error ocurred")
        }
    }


}