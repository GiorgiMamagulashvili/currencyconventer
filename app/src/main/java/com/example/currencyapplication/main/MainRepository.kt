package com.example.currencyapplication.main

import com.example.currencyapplication.retrofit.model.CurrencyResponse
import com.example.currencyapplication.util.Resource

interface MainRepository {

    suspend fun getCurrency(base:String):Resource<CurrencyResponse>

}